# ansible-tmux

install tmux, basic config and plugins

## Role Variables

see `defaults/main.yml`

## Example

```yaml
---
- hosts: all
  roles:
    - tmux
```
